﻿using MessagePostingApp.Services;
 using Microsoft.AspNetCore.Mvc;

namespace MessagePostingApp.Controllers {
    
    [ApiController]
    [Route("api")]
    public class MainController : ControllerBase {
        private readonly MessagesService messagesService;

        public MainController(MessagesService messagesService) {
            this.messagesService = messagesService;
        }

        [HttpGet("json-msg")]
        //GET /api/json-msg
        public IActionResult GetJsonMessages() {
            return new JsonResult(messagesService.GetMessages());
        }
    }
}