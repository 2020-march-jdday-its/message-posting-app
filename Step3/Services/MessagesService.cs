﻿﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
 using MessagePostingApp.Model;

 namespace MessagePostingApp.Services {
    public class MessagesService {
        
        private Queue<Message> Messages;
        private Timer _timer;
        
        public MessagesService() {
            Messages = new Queue<Message>();
            _timer = new Timer(TimerTask, null, 1000, 30000);
        }

        public void AddMessage(Message message) {
            if (Messages.Count >= 50) {
                //Max 50 messages allowed
                Messages.Dequeue();
            }
            Messages.Enqueue(message);
        }

        public IEnumerable GetMessages() {
            return Messages;
        }
        
        private void TimerTask(object timerState) {
            if (Messages.Count > 0) {
                //Delete one message
                Console.WriteLine(Messages.Dequeue());
            }
        }

    }
}