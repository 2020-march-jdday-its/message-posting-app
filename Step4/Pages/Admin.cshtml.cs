﻿using MessagePostingApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace MessagePostingApp.Pages {
    public class AdminModel : PageModel {

        private readonly MessagesService messagesService;

        public AdminModel(MessagesService messagesService) {
            this.messagesService = messagesService;
        }
        
        public IActionResult OnGet() {
            return Page();
        }

        public IActionResult OnPostCleanMessages() {
            messagesService.CleanMessages();
            return Page();
        }
    }
}
