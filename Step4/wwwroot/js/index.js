﻿﻿$(document).ready(function() {
    getMessages();
    setInterval(getMessages, 1000);
});

function getMessages() {
    $.ajax({
        type: "GET",
        url: "/api/json-msg",
        dataType: "text"
    })
        .done(function(response) {
            $("#divMessages").html(buildMessageList(response));
        })
        .fail(function( $xhr ) {

        });//END Ajax call
}

function buildMessageList(messagesJson) {
    var html = '';
    var alertStyles = ['success', 'warning', 'danger', 'primary', 'secondary'];
    if (messagesJson) {
        var messages = JSON.parse(messagesJson);
        var si = 0;
        for (var i = 0; i < messages.length; i++) {
            var m = messages[i];
            html += '<div class="row">';
            html += ' <div class="col-lg-12">';
            html += '  <div class="alert alert-' + alertStyles[si] + '" role="alert">';
            html += '   <h4 class="alert-heading">' + m.name + '</h4>';
            html += '   <p>' + m.msg + '</p>';
            html += '  </div>';
            html += ' </div>';
            html += '</div>';

            if (si >= alertStyles.length) {
                si = 0;
            } else {
                si++;
            }
        }
    }
    return html;
}