﻿﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
 using MessagePostingApp.Model;

 namespace MessagePostingApp.Services {
    public class MessagesService {
        
        private Queue<Message> Messages;
        
        public MessagesService() {
            Messages = new Queue<Message>();
        }

        public void AddMessage(Message message) {
            Messages.Enqueue(message);
        }

        public IEnumerable GetMessages() {
            return Messages;
        }

    }
}